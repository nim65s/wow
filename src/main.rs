use actix_web::{get, middleware, web, App, HttpResponse, HttpServer, Responder};
use listenfd::ListenFd;

extern crate paho_mqtt as mqtt;

use std::sync::Mutex;
use std::time::Duration;

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

struct AppState {
    cli: Mutex<mqtt::AsyncClient>,
}

impl AppState {
    fn new() -> AppState {
        let cli = mqtt::AsyncClientBuilder::new()
            .server_uri("tcp://eclipse-mosquitto:1883")
            .finalize();
        cli.connect(mqtt::ConnectOptions::new())
            .wait_for(Duration::new(5, 0))
            .unwrap();
        let msg = mqtt::Message::new("actix", "hi", 2);
        cli.publish(msg).wait_for(Duration::new(5, 0)).unwrap();
        AppState {
            cli: Mutex::new(cli),
        }
    }
}

impl Drop for AppState {
    fn drop(&mut self) {
        // Create a message and publish it
        let msg = mqtt::Message::new("actix", "bye", 2);
        self.cli
            .lock()
            .unwrap()
            .publish(msg)
            .wait_for(Duration::new(5, 0))
            .unwrap();

        // Disconnect from the broker
        self.cli
            .lock()
            .unwrap()
            .disconnect(mqtt::DisconnectOptions::new())
            .wait_for(Duration::new(5, 0))
            .unwrap();
    }
}

#[get("/mqtt")]
async fn nimqtt(data: web::Data<AppState>) -> impl Responder {
    let cli = data.cli.lock().unwrap();

    // Create a message and publish it
    let msg = mqtt::Message::new("actix", "get /mqtt", 2);
    cli.publish(msg).wait_for(Duration::new(5, 0)).unwrap();

    HttpResponse::Ok().body("sent !")
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let mut listenfd = ListenFd::from_env();

    let appdata = web::Data::new(AppState::new());

    let mut server = HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .app_data(appdata.clone())
            .service(index)
            .service(nimqtt)
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("127.0.0.1:8088")?
    };

    server.run().await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{http::StatusCode, test};

    #[actix_rt::test]
    async fn test_index() {
        let mut app = test::init_service(App::new().service(index)).await;
        let req = test::TestRequest::with_uri("/").to_request();
        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn test_nimqtt() {
        let appdata = web::Data::new(AppState::new());
        let mut app =
            test::init_service(App::new().app_data(appdata.clone()).service(nimqtt)).await;
        let req = test::TestRequest::with_uri("/mqtt").to_request();
        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);
    }
}
